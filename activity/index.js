// console.log("Hey World!");

let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    }
}
console.log(trainer);

trainer.talk = function(){
    console.log("Pikachu! I choose you!");
}
console.log("Result from dot notation:");
console.log(trainer.name);

console.log("Result from square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method");
trainer.talk();

// >>>>>>>>>>>>>>>>>>>>

// 8. Create a constructor for creating a pokemon with the following properties:
// - Name (Provided as an argument to the contructor)
// - Level (Provided as an argument to the contructor)
// - Health (Create an equation that uses the level property)
// - Attack (Create an equation that uses the level property)
// 9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
// 10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
// 11. Create a faint method that will print out a message of targetPokemon has fainted.
// 12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
// 13. Invoke the tackle method of one pokemon object to see if it works as intended.

function Pokemon (name, level){
    this.name = name;
    this.level = level;
    this.health = 2.5 * level;
    this.attack = level / 2;
    this.tackle = function(target){
        
        if (this.health < 0) {
            console.error(`${this.name} already fainted and can no longer attack.
    Choose your next Pokemon for the battle.`)
        } else {
            if (target.health > 0 ){    
                target.health = target.health - this.attack;
                console.log(this.name+ " tackled " + target.name);
                console.log(`${target.name}'s health is now reduced to ${target.health}`);
                console.log(`   * remaining health: ${target.health}  attack: ${target.attack}`);
            } else {
                target.faint(target);
                console.warn(`Choose another Pokemon to attack.`)};
            }        
    };
    this.faint = function(target) {
        console.warn(this.name + " fainted.")};
}
            

let piplup = new Pokemon ("Piplup", 22);
let tepig = new Pokemon ("Tepig", 16);
let oshawott = new Pokemon ("Oshawott", 24);
let fennekin = new Pokemon ("Fennekin", 26);

console.log(piplup);
console.log(tepig);
console.log (oshawott);

// First attack 
piplup.tackle(tepig);
piplup.tackle(tepig);
piplup.tackle(tepig);
tepig.tackle(piplup);
piplup.tackle(tepig);    // 0 index 63

// Tepig down
piplup.tackle(tepig);
piplup.tackle(tepig);
// piplup.tackle(tepig);

// Tepig down, attempt to attack
tepig.tackle(piplup);
tepig.tackle(oshawott);

// Another battle
piplup.tackle(oshawott);
piplup.tackle(fennekin);
oshawott.tackle(piplup);
piplup.tackle(oshawott);
piplup.tackle(oshawott);
oshawott.tackle(fennekin);
oshawott.tackle(piplup);
// oshawott.tackle(piplup);

console.log(piplup);

oshawott.tackle(piplup); 

console.log(piplup);    // -6

oshawott.tackle(piplup);   // Piplup fainted
console.log(oshawott);

// piplup down
piplup.tackle(fennekin);

// Last battle (fennekin vs. oshawott)
console.log(oshawott);
console.log(fennekin);
fennekin.tackle(oshawott);
fennekin.tackle(oshawott);
oshawott.tackle(fennekin);
oshawott.tackle(fennekin);
oshawott.tackle(fennekin);
oshawott.tackle(fennekin);  // -6

oshawott.tackle(fennekin);  // fennekin fainted

fennekin.tackle(oshawott);           
            
            
            
            
            
            
            
            
            
            
            // if (this < 0 = unable to attack
            //     else 
            //         if target < 0 = already fainted 
            //         else target > 0 = use this.attack 
            //         health equation
            //             if target.health > 0 = health is reduced
            //             else target.health < 0 = fainted 
    
            // else if (target.health < 0) {
            //     console.log(`${target.name} is already out of the game`);}

    
    // if (target.health < 0) {
    //     console.error(`${target.name} can no longer attack. Choose another Pokemon.`)
    //     } else {
    //         // if (target.health <= 0) {
    //         //     console.log(`${target.name} already fainted and is now out of the game.`);
    //         } else if (target.health <= 0){
    //                 console.log(`${target.name} is out of the game`);
    //             } else {
    //                 if (target.health > 0 ){
    //                     target.health = target.health - this.attack;
    //                     console.log(this.name+ " tackled " + target.name);
    //                     console.log(`${target.name}'s health is now reduced to ${target.health}`);
    //                     console.warn(`(${target.name}) health:${target.health}  attack:${target.attack}`);
    //                 } else {
    //                     target.faint(target);
    //                 }
        
